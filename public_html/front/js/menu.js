menu = (function() {
    var menuIcon = document.getElementById('hamburger');
    var menu = document.getElementById('menu');
    menuIcon.addEventListener("click", function() {
        if (menu.classList.contains('visible')) {
            menu.classList.remove('visible');
        } else {
            menu.classList.add('visible');
        }
    });
})();