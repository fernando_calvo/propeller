carousel = (function() {
    this.buttons = document.getElementsByClassName('carousel-btn');
    this.images = document.getElementsByClassName('carousel-img');
    this.currentImg = document.getElementsByClassName('carousel-img current')[0];
    this.currentBtn = document.getElementsByClassName('carousel-btn current')[0];
    for (var i=0; i<this.buttons.length; i++) {
        buttons[i].addEventListener('click', function(event) {
            navigate(parseInt(event.currentTarget.id));
        });
    }
    var navigate = function(index) {
        this.currentImg.classList.remove('current');
        this.currentImg = this.images[index];
        this.currentImg.classList.add('current');
        
        this.currentBtn.children[0].classList.remove('icon-carousel-current');
        this.currentBtn.children[0].classList.add('icon-carousel');
        this.currentBtn = this.buttons[index];
        this.currentBtn.children[0].classList.add('icon-carousel-current');
        this.currentBtn.children[0].classList.remove('icon-carousel');
    }.bind(this);
})();