/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
module.exports = function (grunt) {
    grunt.initConfig({
        less: {
            development: {
                options: {
                    paths: ["public_html/front/less"]
                },
                files: {"public_html/front/css/styles.css": "public_html/front/less/styles.less"}
            },
            production: {
                options: {
                    paths: ["assets/css"],
                    cleancss: true
                },
                files: {"path/to/result.css": "path/to/source.less"}
            }
        },
        watch: {
            scripts: {
                files: ['public_html/front/less/*'],
                tasks: ['less:development'],
                options: {
                    spawn: false
                }
            }
        },
        sprite: {
            all: {
                src: 'public_html/front/images/spritesheet/*.png',
                dest: 'public_html/front/images/spritesheet.png',
                destCss: 'public_html/front/css/sprites.css'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-spritesmith');
};
